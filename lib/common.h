#pragma once

#include "fifo.h"
#include <stdio.h>

#ifdef DAL_API
#include "dal.h"

typedef DALProcess *PROCESS_PTR_T;
typedef int PROCESS_RETURN_T;
#else
typedef void *PROCESS_PTR_T;
typedef void *PROCESS_RETURN_T;
#endif

#define MAX_PORTS 32

typedef struct {
	cl_context context;
	cl_mem_flags flags;
} cl_mem_params_t;

typedef struct {
	fifo_t **inputs;
	fifo_t **outputs;
} shared_t;

typedef struct {
	cl_command_queue commands;
	cl_kernel kernel;
	cl_mem *constantData;
	int constantDataCount;
	size_t *globalSize;
	size_t *localSize;
	int dimensions;
	int isDynamic;
	PROCESS_RETURN_T (*ioFunction)(PROCESS_PTR_T);
} gpu_actor_params_t;

typedef struct {
	PROCESS_RETURN_T (*function)(PROCESS_PTR_T);
	PROCESS_PTR_T appData;
	shared_t shared;
} cpu_actor_params_t;

typedef struct {
	int inputEnabled[MAX_PORTS];
	int outputEnabled[MAX_PORTS];
	int cValue;
} io_data_t;

int getFileSize(FILE *fp);

/**
 * \brief Retrieves the user-specified location of <filename>.
 *
 * Retrieves the full path of the input file with the given name based on a
 * user-specified preprocessor directive (that originates in cmake). Falls back
 * on home directory.
 *
 * \param filename is the name of the required file
 * \param full path is the output variable
 */
void writeInputFilepath(const char *filename, char *fullpath);

/**
 * \brief Retrieves the user-specified output file for <filename>.
 *
 * Retrieves the full path to the output file with the given name based on a
 * user-specified preprocessor directive (that originates in cmake). Falls back
 * on home directory.
 *
 * \param filename is the name of the output file
 * \param full path is the output variable
 */
void writeOutputFilepath(const char *filename, char *fullpath);

/**
 * \brief Opens a file-handle to the file with filename. Note that this file is
 * searched for using writeInputFilepath.
 *
 * \param filename is the name of the required file
 * \sa writeInputFilepath
 */
FILE *fopenInput(const char *filename);

/**
 * \brief Opens a file-handle to the file with filename. Note that this file is
 * searched for using writeOutputFilepath.
 *
 * \param filename is the name of the output file
 * \sa writeOutputFilepath
 */
FILE *fopenOutput(const char *filename);

int readRawData(char *fn, void **wgt);
int readConstant(char *fn, void *val, int size);
void actorTerminate();

#ifdef DAL_API
int fileRead(FILE *file, cl_float *buffer, int *count, int length);
DALProcess makeDalProcess(void *local, shared_t *shared);
#endif
