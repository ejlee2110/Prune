#include "dalSupport.h"
#include "common.h"
#include "dal.h"
#include "fifo.h"
#include <stdio.h>

// Default FIFO interface
int channel_write(void *port, void *buf, int len, DALProcess *p) {
	printf("channel_write is not yet implemented!\n");
	return 0;
}
int channel_read(void *port, void *buf, int len, DALProcess *p) {
	printf("channel_read is not yet implemented!\n");
	return 0;
}

// Windowed FIFO interface
void *channel_read_begin(int port, int tokensize, int tokenrate, DALProcess *p) {
	shared_t *shared = (shared_t*)p->wptr;
	fifo_t *inputs = shared->inputs[port];
	return (void *)fifoReadStart(inputs);
}
void channel_read_end(int port, void *buf, DALProcess *p) {
	shared_t *shared = (shared_t*)p->wptr;
	fifo_t *inputs = shared->inputs[port];
	fifoReadEnd(inputs);
}
void *channel_write_begin(int port, int tokensize, int tokenrate, int blocksize, int blk_ptr,
						  DALProcess *p) {
	shared_t *shared = (shared_t*)p->wptr;
	fifo_t *outputs = shared->outputs[port];
	return (void *)fifoWriteStart(outputs);
}
void channel_write_end(int port, void *buf, DALProcess *p) {
	shared_t *shared = (shared_t*)p->wptr;
	fifo_t *outputs = shared->outputs[port];
	fifoWriteEnd(outputs);
}
