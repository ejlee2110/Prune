/*! \file dalMacros.h
 \brief Define main channel functions

 \authors Lothar Thiele, Lars Schor, Devendra Rai
 */

#ifndef __DALSUPPORT_H
#define __DALSUPPORT_H

typedef struct _process DALProcess;

// Default FIFO interface
int channel_write(void *port, void *buf, int len, DALProcess *p);
int channel_read(void *port, void *buf, int len, DALProcess *p);

// Windowed FIFO interface
void *channel_read_begin(int port, int tokensize, int tokenrate, DALProcess *p);
void channel_read_end(int port, void *buf, DALProcess *p);
void *channel_write_begin(int port, int tokensize, int tokenrate, int blocksize, int blk_ptr,
						  DALProcess *p);
void channel_write_end(int port, void *buf, DALProcess *p);

#define DAL_write(port, buf, len, p) channel_write(port, buf, len, p)

#define DAL_read(port, buf, len, p) channel_read(port, buf, len, p)

#define DAL_read_begin(port, tokensize, tokenrate, p)                                              \
	channel_read_begin(port, tokensize, tokenrate, p)
#define DAL_read_end(port, buf, p) channel_read_end(port, buf, p)
#define DAL_write_begin(port, tokensize, tokenrate, blocksize, blk_ptr, p)                         \
	channel_write_begin(port, tokensize, tokenrate, blocksize, blk_ptr, p)
#define DAL_write_end(port, buf, p) channel_write_end(port, buf, p)

#endif
