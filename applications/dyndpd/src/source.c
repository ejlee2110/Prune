#include <stdio.h>
#include <string.h>
#include "source.h"

extern volatile int globalRepetitions;

int fileRead (FILE *file, cl_float *buffer, int *count, int length) {
	int cnt = -1;
	if (file != NULL) {
		cnt = fread(buffer, sizeof(cl_float), length, file);
		count[0] += cnt;
	}
	return cnt;
}

int sourceInit(source_data_t *data) {
	data->file = NULL;
	data->count = 0;
	data->length = TOKEN_SIZE - (NUM_TAPS-1);
	for(int i = 0; i < NUM_TAPS-1; i++) {
		data->tbuffer[i] = 0.0;
	}

	char sourceFileName[256];
	sprintf(sourceFileName, "%s.bin", data->fn);
	data->file = fopenInput(sourceFileName);
	if (!data->file) {
		printf("Error: could not open input file %s\n", sourceFileName);
	}

	globalRepetitions = getFileSize(data->file) / (sizeof(float) * data->length);
	data->iteration = 0;
	// FIXME: what should this function return?
	return 0;
}

void *sourceFire(void *p) {
	source_data_t *data = (source_data_t *) p;
	if(data->iteration >= globalRepetitions) {
		printf("Source finished\n");
		actorTerminate();
	} else if (data->file != NULL) {
		fifo_t *output = data->shared->outputs[0];
		cl_float *token = (cl_float *) fifoWriteStart(output);
		memcpy(token, data->tbuffer, sizeof(cl_float)*(NUM_TAPS-1));
		fileRead(data->file, &token[NUM_TAPS-1], &data->count, data->length);
		memcpy(data->tbuffer, &token[data->length], sizeof(cl_float)*(NUM_TAPS-1));
		fifoWriteEnd(output);
		data->iteration ++;
	}

	return p;
}

void sourceFinish(source_data_t *data) {
	if (data->file != NULL) {
		fclose(data->file);
		data->file = NULL;
	}
}

