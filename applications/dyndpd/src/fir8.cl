#include "fir.h"
#include "ports.h"

#define fir8_i_out_type float*
#define fir8_q_out_type float*
#define fir8_i_in_type float*
#define fir8_q_in_type float*

__kernel void fir8(
	__global fir8_input_0_type restrict fir8_input_0_name,
	__global fir8_input_1_type restrict fir8_input_1_name,
	__global fir8_output_0_type restrict fir8_output_0_name,
	__global fir8_output_1_type restrict fir8_output_1_name) {

	const float fir_ic[NUM_TAPS] = 
		{-0.010361481457948685, -0.0004938115598633885, 0.004675863776355982, -0.003926571924239397, 0.0046980371698737144, -0.006110482849180698, 0.006143227219581604, -0.004450784996151924, 0.002773092593997717, -0.0021631026174873114}; 
	const float fir_qc[NUM_TAPS] = 
		{0.003214986063539982, -0.004554244689643383, 0.0044721211306750774, -0.0006862758309580386, -0.0020631044171750546, 0.002121124416589737, -0.0013605181593447924, -0.0001483698288211599, 0.001545978942885995, -0.001356405089609325}; 

	int j = get_global_id(0);  

	float io_tmp = 0.0;
	float qo_tmp = 0.0;
	for (int t = 0; t < NUM_TAPS; t++) {
		float tmp_i_in = fir8_i_in[j+t];
		float tmp_q_in = fir8_q_in[j+t];
		float tmp_qc = fir_qc[NUM_TAPS - 1 - t];
		float tmp_ic = fir_ic[NUM_TAPS - 1 - t];
		io_tmp = io_tmp + tmp_i_in * tmp_ic - tmp_q_in * tmp_qc;
		qo_tmp = qo_tmp + tmp_i_in * tmp_qc + tmp_q_in * tmp_ic;
	}
	fir8_i_out[j] = io_tmp;
	fir8_q_out[j] = qo_tmp;
}

