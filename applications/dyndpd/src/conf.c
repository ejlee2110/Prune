#include "dyndpd.h"
#include "conf.h"
#include "ports.h"

int confInit(conf_data_t *data) {
	data->iFile = NULL;
	data->iLength = 1;

	char confIFileName[256];
	sprintf(confIFileName, "i%s.bin", data->fn);
	data->iFile = fopenInput(confIFileName);
	if (!data->iFile) {
		printf("Error: could not open file %s\n", confIFileName);
	}

	data->qFile = NULL;
	data->qLength = 1;

	char confQFileName[256];
	sprintf(confQFileName, "q%s.bin", data->fn);
	data->qFile = fopenInput(confQFileName);
	if (!data->qFile) {
		printf("Error: could not open file %s\n", confQFileName);
	}
	return 0;
}

void *confFire(void *p) {
	cl_int iTmp = 0, qTmp = 0;
	conf_data_t *data = (conf_data_t *) p;

	fifo_t *iOutput1 = data->shared->outputs[CONF_N_OUT1_0];
	fifo_t *iOutput2 = data->shared->outputs[CONF_N_OUT1_1];
	cl_int *iToken1 = (cl_int *) fifoWriteStart(iOutput1);
	cl_int *iToken2 = (cl_int *) fifoWriteStart(iOutput2);

	int retval = fread(&iTmp, sizeof(cl_char), data->iLength, data->iFile);
	if (retval != data->iLength) {
		printf("Source i%s.bin depleted\n", data->fn);
	}
	retval = fread(&qTmp, sizeof(cl_char), data->qLength, data->qFile);
	if (retval != data->qLength) {
		printf("Source q%s.bin depleted\n", data->fn);
	}
	int word = (iTmp << 8) + qTmp;
	
	iToken1[0] = word;
	iToken2[0] = word;

	fifoWriteEnd(iOutput1);
	fifoWriteEnd(iOutput2);

	return p;
}

void confFinish(conf_data_t *data) {
	if (data->iFile != NULL) {
		fclose(data->iFile);
		data->iFile = NULL;
	}
	if (data->qFile != NULL) {
		fclose(data->qFile);
		data->qFile = NULL;
	}
}

