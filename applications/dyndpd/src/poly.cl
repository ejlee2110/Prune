#include "fir.h"
#include "ports.h"

#define NORM 5

#define poly_n_c_type int*
#define poly_i_in_type float*
#define poly_q_in_type float*
#define poly_i_out1_type float*
#define poly_q_out1_type float*
#define poly_i_out2_type float*
#define poly_q_out2_type float*
#define poly_i_out3_type float*
#define poly_q_out3_type float*
#define poly_i_out4_type float*
#define poly_q_out4_type float*
#define poly_i_out5_type float*
#define poly_q_out5_type float*
#define poly_i_out6_type float*
#define poly_q_out6_type float*
#define poly_i_out7_type float*
#define poly_q_out7_type float*
#define poly_i_out8_type float*
#define poly_q_out8_type float*
#define poly_i_out9_type float*
#define poly_q_out9_type float*
#define poly_i_outA_type float*
#define poly_q_outA_type float*

__kernel void poly(
	__global poly_input_0_type restrict poly_input_0_name,
	__global poly_input_1_type restrict poly_input_1_name,
	__global poly_input_2_type restrict poly_input_2_name,
	__global poly_output_0_type restrict poly_output_0_name,
	__global poly_output_1_type restrict poly_output_1_name,
	__global poly_output_2_type restrict poly_output_2_name,
	__global poly_output_3_type restrict poly_output_3_name,
	__global poly_output_4_type restrict poly_output_4_name,
	__global poly_output_5_type restrict poly_output_5_name,
	__global poly_output_6_type restrict poly_output_6_name,
	__global poly_output_7_type restrict poly_output_7_name,
	__global poly_output_8_type restrict poly_output_8_name,
	__global poly_output_9_type restrict poly_output_9_name,
	__global poly_output_10_type restrict poly_output_10_name,
	__global poly_output_11_type restrict poly_output_11_name,
	__global poly_output_12_type restrict poly_output_12_name,
	__global poly_output_13_type restrict poly_output_13_name,
	__global poly_output_14_type restrict poly_output_14_name,
	__global poly_output_15_type restrict poly_output_15_name,
	__global poly_output_16_type restrict poly_output_16_name,
	__global poly_output_17_type restrict poly_output_17_name,
	__global poly_output_18_type restrict poly_output_18_name,
	__global poly_output_19_type restrict poly_output_19_name) {

	int n_ind;
	int c_ind;
	float polytab[NORM];

	float abs_sqr_o;
	int j;
	float tmp_ivect, tmp_qvect;
	int i;

	n_ind = poly_n_c[0] >> 8;
	c_ind = poly_n_c[0] & 0xFF;

	j = get_global_id(0);  

	tmp_ivect = poly_i_in[j];
	tmp_qvect = poly_q_in[j];
	abs_sqr_o = tmp_ivect * tmp_ivect + tmp_qvect * tmp_qvect;
	polytab[0] = 1.0;
	for(i = 1; i < NORM; i ++) {
		polytab[i] = polytab[i - 1] * abs_sqr_o;
	}

	if (n_ind == 5) {
		poly_i_out5[j] = tmp_ivect * polytab[4];
		poly_q_out5[j] = tmp_qvect * polytab[4];
	}
	if (n_ind >= 4) {
		poly_i_out4[j] = tmp_ivect * polytab[3];
		poly_q_out4[j] = tmp_qvect * polytab[3];
	}
	if (n_ind >= 3) {
		poly_i_out3[j] = tmp_ivect * polytab[2];
		poly_q_out3[j] = tmp_qvect * polytab[2];
	}
	if (n_ind >= 2) {
		poly_i_out2[j] = tmp_ivect * polytab[1];
		poly_q_out2[j] = tmp_qvect * polytab[1];
	}
	if (n_ind >= 1) {
		poly_i_out1[j] = tmp_ivect * polytab[0];
		poly_q_out1[j] = tmp_qvect * polytab[0];
	}

	if (c_ind == 5) {
		poly_i_outA[j] = tmp_ivect * polytab[4];
		poly_q_outA[j] = -(tmp_qvect * polytab[4]);
	}
	if (c_ind >= 4) {
		poly_i_out9[j] = tmp_ivect * polytab[3];
		poly_q_out9[j] = -(tmp_qvect * polytab[3]);
	}
	if (c_ind >= 3) {
		poly_i_out8[j] = tmp_ivect * polytab[2];
		poly_q_out8[j] = -(tmp_qvect * polytab[2]);
	}
	if (c_ind >= 2) {
		poly_i_out7[j] = tmp_ivect * polytab[1];
		poly_q_out7[j] = -(tmp_qvect * polytab[1]);
	}
	if (c_ind >= 1) {
		poly_i_out6[j] = tmp_ivect * polytab[0];
		poly_q_out6[j] = -(tmp_qvect * polytab[0]);
	}
}


