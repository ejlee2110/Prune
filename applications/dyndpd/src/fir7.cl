#include "fir.h"
#include "ports.h"

#define fir7_i_out_type float*
#define fir7_q_out_type float*
#define fir7_i_in_type float*
#define fir7_q_in_type float*

__kernel void fir7(
	__global fir7_input_0_type restrict fir7_input_0_name,
	__global fir7_input_1_type restrict fir7_input_1_name,
	__global fir7_output_0_type restrict fir7_output_0_name,
	__global fir7_output_1_type restrict fir7_output_1_name) {

	const float fir_ic[NUM_TAPS] = 
		{0.009307628497481346, -0.009812917560338974, 0.01985200308263302, -0.03114335797727108, 0.03306770697236061, -0.02422008290886879, 0.011776125989854336, -0.0031052138656377792, -0.00022539039491675794, 0.000567929819226265}; 
	const float fir_qc[NUM_TAPS] = 
		{0.006318103522062302, -0.011036279611289501, 0.026539787650108337, -0.04439254477620125, 0.049906931817531586, -0.0384700670838356, 0.01996430568397045, -0.006266576237976551, 0.0007132088067010045, 0.00015458985581062734}; 

	int j = get_global_id(0);  

	float io_tmp = 0.0;
	float qo_tmp = 0.0;
	for (int t = 0; t < NUM_TAPS; t++) {
		float tmp_i_in = fir7_i_in[j+t];
		float tmp_q_in = fir7_q_in[j+t];
		float tmp_qc = fir_qc[NUM_TAPS - 1 - t];
		float tmp_ic = fir_ic[NUM_TAPS - 1 - t];
		io_tmp = io_tmp + tmp_i_in * tmp_ic - tmp_q_in * tmp_qc;
		qo_tmp = qo_tmp + tmp_i_in * tmp_qc + tmp_q_in * tmp_ic;
	}
	fir7_i_out[j] = io_tmp;
	fir7_q_out[j] = qo_tmp;
}


