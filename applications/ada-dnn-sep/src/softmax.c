#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "dnn.h"
#include "softmax.h"
#include "ports.h"

extern volatile int globalApplicationFinished;
extern volatile int globalRepetitions;

void lide_c_mtx_mulf(cl_float* A, cl_float* B, cl_float* c_mul, int Mdim, int Ndim, int Kdim) {
    int i, j, z;
	memset (c_mul, 0, sizeof(cl_float) * Mdim * Kdim);
    for (i = 0; i < Mdim; i++)
    {
        for (j = 0; j < Kdim; j++)
        {
            for (z = 0; z < Ndim; z++)
            {
                matrix(c_mul, Kdim, i, j) += matrix(A, Ndim, i, z) * matrix(B, Kdim, z, j);
            }
        }
    }
}

void lide_c_image_relu(cl_float *bef_array, cl_float *aft_array, int row, int column) {
    int i,j;
    for (i = 0; i < row; i++)
    {
        for (j = 0; j < column; j++)
        {
            cl_float tmp = 0;
            tmp = tmp > matrix(bef_array, column, i, j)? tmp : matrix(bef_array, column, i, j);
            matrix(aft_array, column, i, j) = tmp;
        }

    }
}

void lide_c_softmax_write(cl_float *input_array, cl_float *output_array, int m, int n) {
    int i,j;
    int sum = 0;
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
            sum += exp(matrix(input_array, n, i, j));
        }
    }
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
			output_array[i] = exp(matrix(input_array, n, i, j))/sum;
        }
    }
}

void softmaxInit(softmax_data_t *data) {
	char fileName[256];
	sprintf(fileName, "%s/%s.bin", getenv("HOME"), "mulL4");
	readRawData (fileName, (void **) &data->coeff_l4);
	sprintf(fileName, "%s/%s.bin", getenv("HOME"), "mulL5");
	readRawData (fileName, (void **) &data->coeff_l5);
	data->iteration = 0;
}

void *softmaxFire(void *p) {
	softmax_data_t *data = (softmax_data_t *) p;
	cl_float fifo_relu_out_1[MAGIC];
	cl_float fifo_multi_out_2[MAGIC];
	cl_float fifo_relu_out_2[MAGIC];
	cl_float fifo_multi_out_3[CLASSES];
	int *select = (int *) fifoReadStart(data->shared->inputs[SOFTMAXL5_IN3]);

	cl_float *output = (cl_float *) fifoWriteStart(data->shared->outputs[0]);
	if (select[0] == 1) {
		cl_float *input1 = (cl_float *) fifoReadStart(data->shared->inputs[SOFTMAXL5_IN1]);
		for (int i = 0; i < REPEAT; i++) {
			lide_c_image_relu(&input1[i*TOKEN4SIZE], fifo_relu_out_1, MAGIC, 1);

			// LAYER 4
			lide_c_mtx_mulf(data->coeff_l4, fifo_relu_out_1, fifo_multi_out_2, MAGIC, MAGIC, 1);
			lide_c_image_relu(fifo_multi_out_2, fifo_relu_out_2, MAGIC, 1);

			// LAYER 5
			lide_c_mtx_mulf(data->coeff_l5, fifo_relu_out_2, fifo_multi_out_3, CLASSES, MAGIC, 1);
			lide_c_softmax_write (fifo_multi_out_3, &output[i*TOKEN5SIZE], CLASSES, 1);
		}
		fifoReadEnd(data->shared->inputs[SOFTMAXL5_IN1]);
	} else {
		cl_float *input2 = (cl_float *) fifoReadStart(data->shared->inputs[SOFTMAXL5_IN2]);
		for (int i = 0; i < REPEAT; i++) {
			for (int j = 0; j < CLASSES; j++){
				output[i*TOKEN5SIZE + j] = input2[0];
			}
		}
		fifoReadEnd(data->shared->inputs[SOFTMAXL5_IN2]);
	}
	fifoReadEnd(data->shared->inputs[SOFTMAXL5_IN3]);
	fifoWriteEnd(data->shared->outputs[0]);

	return p;
}

void softmaxFinish(softmax_data_t *data) {
	free (data->coeff_l4);
	free (data->coeff_l5);
}

