#include "fig4app.h"
#include "x.h"
#include <stdlib.h>
#include <string.h>
#include "ports.h"

void xInit(x_data_t *data) {
}

void *xFire(void *p) {
	x_data_t *data = (x_data_t *) p;
	#ifdef DBGPRINT
	printf("actor x fires\n");
	#endif
	int *rbufin1 = (int *) fifoReadStart(data->shared->inputs[X_IN1]);
	float *rbufin2 = (float *) fifoReadStart(data->shared->inputs[X_IN2]);
	if (rbufin1[0] == 1) {
		float *wbufout1 = (float *) fifoWriteStart(data->shared->outputs[X_OUT1]);
		float *wbufout2 = (float *) fifoWriteStart(data->shared->outputs[X_OUT2]);
		wbufout1[0] = rbufin2[0];
		wbufout2[0] = rbufin2[0];
		fifoWriteEnd(data->shared->outputs[X_OUT1]);
		fifoWriteEnd(data->shared->outputs[X_OUT2]);
	} else if (rbufin1[0] == 2) {
		float *wbufout = (float *) fifoWriteStart(data->shared->outputs[X_OUT3]);
		wbufout[0] = rbufin2[0];
		fifoWriteEnd(data->shared->outputs[X_OUT3]);
	} else if (rbufin1[0] == 3) {
		float *wbufout = (float *) fifoWriteStart(data->shared->outputs[X_OUT4]);
		wbufout[0] = rbufin2[0];
		fifoWriteEnd(data->shared->outputs[X_OUT4]);
	} else {
		printf("actor x received unknown conf value %i\n", rbufin1[0]);
	}

	fifoReadEnd(data->shared->inputs[X_IN1]);
	fifoReadEnd(data->shared->inputs[X_IN2]);

	return p;
}

void xFinish(x_data_t *data) {
}
