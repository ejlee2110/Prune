#include "fig4app.h"
#include "y.h"
#include <stdlib.h>
#include <string.h>
#include "ports.h"

void yInit(y_data_t *data) {
}

void *yFire(void *p) {
	y_data_t *data = (y_data_t *) p;
	#ifdef DBGPRINT
	printf("actor y fires\n");
	#endif
	int *rbufin1 = (int *) fifoReadStart(data->shared->inputs[Y_IN1]);
	float *wbufout = (float *) fifoWriteStart(data->shared->outputs[Y_OUT1]);
	if (rbufin1[0] == 1) {
		float *rbufin = (float *) fifoReadStart(data->shared->inputs[Y_IN2]);
		wbufout[0] = rbufin[0];
		fifoReadEnd(data->shared->inputs[Y_IN2]);
	} else if (rbufin1[0] == 2) {
		float *rbufin = (float *) fifoReadStart(data->shared->inputs[Y_IN3]);
		wbufout[0] = rbufin[0];
		fifoReadEnd(data->shared->inputs[Y_IN3]);
	} else if (rbufin1[0] == 3) {
		float *rbufin = (float *) fifoReadStart(data->shared->inputs[Y_IN4]);
		wbufout[0] = rbufin[0];
		fifoReadEnd(data->shared->inputs[Y_IN4]);
	} else {
		printf("actor y received unknown conf value %i\n", rbufin1[0]);
	}

	fifoReadEnd(data->shared->inputs[Y_IN1]);
	fifoWriteEnd(data->shared->outputs[Y_OUT1]);

	return p;
}

void yFinish(y_data_t *data) {
}
