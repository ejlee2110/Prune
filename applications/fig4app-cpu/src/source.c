#include "fig4app.h"
#include "source.h"
#include <stdlib.h>
#include <string.h>

extern volatile int globalRepetitions;

void sourceInit(source_data_t *data) {
	char sourceFileName[256];
	data->file = NULL;
	sprintf(sourceFileName, "data/%s.bin", data->fn);
	data->file = fopen(sourceFileName, "rb");
	if (data->file == NULL) {
		printf("Could not open %s\n", sourceFileName);
		return;
	}
	globalRepetitions = getFileSize(data->file) / sizeof(float);
	data->iteration = 0;
	printf("Running for %i numbers\n", globalRepetitions);
}

void *sourceFire(void *p) {
	source_data_t *data = (source_data_t *) p;
	if(data->iteration >= globalRepetitions) {
		printf("Source finished\n");
		actorTerminate();
	} else if (data->file != NULL) {
		float *wbufout1 = (float *) fifoWriteStart(data->shared->outputs[0]);
		#ifdef DBGPRINT
		printf("actor source fires\n");
		#endif
		int retval = fread(wbufout1, sizeof(cl_float), 1, data->file);
		if (retval != 1) {
			printf("Source %s depleted\n", data->fn);
		}
		fifoWriteEnd(data->shared->outputs[0]);
		data->iteration ++;
	}

	return p;
}

void sourceFinish(source_data_t *data) {
	if (data->file != NULL) {
		fclose(data->file);
		data->file = NULL;
	}
}
