#include "fig4app.h"
#include "q.h"
#include <stdlib.h>
#include <string.h>

void qInit(q_data_t *data) {
	char qFileName[256];
	data->file = NULL;
	sprintf(qFileName, "data/%s.bin", data->fn);
	data->iteration = 0;
	data->file = fopen(qFileName, "r");
	if (data->file == NULL) {
		printf("Could not open %s\n", qFileName);
		return;
	}
}

void *qFire(void *p) {
	q_data_t *data = (q_data_t *) p;
	#ifdef DBGPRINT
	printf("actor q fires\n");
	#endif
	char cenable;
	int enable;
	int retval = fread(&cenable, sizeof(char), 1, data->file);
	if (retval != 1) {
		printf("Source %s depleted\n", data->fn);
	}
	enable = (int) cenable;
	int *wbufout1 = (int *) fifoWriteStart(data->shared->outputs[0]);
	int *wbufout2 = (int *) fifoWriteStart(data->shared->outputs[1]);
	wbufout1[0] = enable;
	wbufout2[0] = enable;
	fifoWriteEnd(data->shared->outputs[0]);
	fifoWriteEnd(data->shared->outputs[1]);

	return p;
}

void qFinish(q_data_t *data) {
	if (data->file != NULL) {
		fclose(data->file);
		data->file = NULL;
	}
}
