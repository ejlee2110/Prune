#pragma once

#include "common.h"

typedef struct {
	shared_t *shared;
} a4_data_t;

void a4Init(a4_data_t *data);
void *a4Fire(void *p);
void a4Finish(a4_data_t *data);
