#include "fig4app.h"
#include "a2.h"
#include "ports.h"

void a2Init(a2_data_t *data) {
}

void a2Finish(a2_data_t *data) {
}

void *a2Fire(void *p) {
	a2_data_t *data = (a2_data_t *) p;
	cl_float *input1 = (cl_float *) fifoReadStart(data->shared->inputs[A2_IN1]);
	cl_float *input2 = (cl_float *) fifoReadStart(data->shared->inputs[A2_IN2]);
	cl_float *output = (cl_float *) fifoWriteStart(data->shared->outputs[0]);
	#ifdef DBGPRINT
	printf("actor a2 fires\n");
	#endif

	output[0] = input1[0] + input2[0];

	fifoReadEnd(data->shared->inputs[A2_IN1]);
	fifoReadEnd(data->shared->inputs[A2_IN2]);
	fifoWriteEnd(data->shared->outputs[0]);

	return p;
}
