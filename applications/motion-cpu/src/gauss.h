#include <stdio.h>
#include "common.h"

typedef struct {
	shared_t *shared;
} gauss_data_t;

int gaussInit(gauss_data_t *data);
void *gaussFire(void *p);
void gaussFinish(gauss_data_t *data);

