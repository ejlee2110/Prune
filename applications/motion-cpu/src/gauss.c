#include "motion.h"
#include "gauss.h"

int gaussInit(gauss_data_t *data) {
	return 0;
}

void gaussFinish(gauss_data_t *data) {
}

void *gaussFire(void *p) {
	
	const  int gauss_gaussian[25] = {1, 3, 6, 3, 1, 3, 15, 25, 15, 3, 6, 25, 41, 26, 6, 3, 15, 25, 15, 3, 1, 3, 6, 3, 1};
	int idx;
	int tmpgauss;
	int j, k;
	int tmp_gaussian;
	unsigned char tmp_in1;
	int tmp_gaussian0;
	unsigned char tmp_in10;
	int tmp_gaussian1;
	unsigned char tmp_in11;
	int tmp_gaussian2;
	unsigned char tmp_in12;
	int tmp_gaussian3;
	unsigned char tmp_in13;

	gauss_data_t *data = (gauss_data_t *) p;

	unsigned char *rbufin1 = (unsigned char *) fifoReadStart(data->shared->inputs[0]);
	unsigned char *wbufout1 = (unsigned char *) fifoWriteStart(data->shared->outputs[0]);
	unsigned char *wbufout2 = (unsigned char *) fifoWriteStart(data->shared->outputs[1]);

	for (j = 0; j < PROCESSSIZE; j++)
	{
		if ((j < 2 + 2 * WIDTH || j + 2 + 2 * WIDTH > PROCESSSIZE)) {
			idx = 2 + 2 * WIDTH;
		} else {
			idx = j;
		}
		tmpgauss = 0;
		k = -2;
		while (k <= 2) {
			tmp_gaussian = gauss_gaussian[(k + 2) * 5 + 0];
			tmp_in1 = rbufin1[idx - 2 + k * WIDTH];
			tmp_gaussian0 = gauss_gaussian[(k + 2) * 5 + 1];
			tmp_in10 = rbufin1[idx - 1 + k * WIDTH];
			tmp_gaussian1 = gauss_gaussian[(k + 2) * 5 + 2];
			tmp_in11 = rbufin1[idx + k * WIDTH];
			tmp_gaussian2 = gauss_gaussian[(k + 2) * 5 + 3];
			tmp_in12 = rbufin1[idx + 1 + k * WIDTH];
			tmp_gaussian3 = gauss_gaussian[(k + 2) * 5 + 4];
			tmp_in13 = rbufin1[idx + 2 + k * WIDTH];
			tmpgauss = tmpgauss + tmp_gaussian * tmp_in1 + tmp_gaussian0 * tmp_in10 + tmp_gaussian1 * tmp_in11 + tmp_gaussian2 * tmp_in12 + tmp_gaussian3 * tmp_in13;
			k = k + 1;
		}
		wbufout1[idx] = tmpgauss >> 8;
		wbufout2[idx] = tmpgauss >> 8;
	}

	fifoReadEnd(data->shared->inputs[0]);
	fifoWriteEnd(data->shared->outputs[0]);
	fifoWriteEnd(data->shared->outputs[1]);

	return p;
}

