#include <stdio.h>
#include "common.h"

typedef struct {
	shared_t *shared;
} thres_data_t;

int thresInit(thres_data_t *data);
void *thresFire(void *p);
void thresFinish(thres_data_t *data);

