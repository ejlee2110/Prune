#include "motion.h"
#include "thres.h"

int thresInit(thres_data_t *data) {
	return 0;
}

void thresFinish(thres_data_t *data) {

}
	
void *thresFire(void *p) {

	int idx;
	int tmp;
	int t1;
	int t2;
	int j;

	thres_data_t *data = (thres_data_t *) p;

	unsigned char *rbufin1 = (unsigned char *) fifoReadStart(data->shared->inputs[0]);
	unsigned char *rbufin2 = (unsigned char *) fifoReadStart(data->shared->inputs[1]);
	unsigned char *wbufout1 = (unsigned char *) fifoWriteStart(data->shared->outputs[0]);

	for (j = 0; j < PROCESSSIZE; j++)
	{
		idx = j;
		t1 = rbufin1[idx];
		t2 = rbufin2[idx];
		tmp = t1 - t2;
		if (tmp < 0) {
			tmp = -tmp;
		}
		if (tmp > THRESHOLD) {
			tmp = 255;
		} else {
			tmp = 0;
		}
		wbufout1[idx] = tmp;
	}

	fifoReadEnd(data->shared->inputs[0]);
	fifoReadEnd(data->shared->inputs[1]);
	fifoWriteEnd(data->shared->outputs[0]);

	return p;
}

