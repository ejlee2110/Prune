#include "ports.h"

#define a2_in1_type float*
#define a2_in2_type float*
#define a2_out1_type float*

__kernel void a2(
	__global a2_input_0_type restrict a2_input_0_name,
	__global a2_input_1_type restrict a2_input_1_name,
	__global a2_output_0_type restrict a2_output_0_name) {
	a2_out1[0] = a2_in1[0] + a2_in2[0];
}
