#include "dyndpd.h"
#include "fir6.h"
#include "ports.h"

int fir6Init(fir6_data_t *data) {
	return 0;
}

void fir6Finish(fir6_data_t *data) {
}

void *fir6Fire(void *p) {

	fir6_data_t *data = (fir6_data_t *) p;
	float *iTokenIn = (float *) fifoReadStart(data->shared->inputs[FIR6_I_IN]);
	float *qTokenIn = (float *) fifoReadStart(data->shared->inputs[FIR6_Q_IN]);
	float *iTokenOut = (float *) fifoWriteStart(data->shared->outputs[FIR6_I_OUT]);
	float *qTokenOut = (float *) fifoWriteStart(data->shared->outputs[FIR6_Q_OUT]);

	const float fir_ic[NUM_TAPS] = 
		{0.03875238075852394, -0.025881294161081314, 0.019611205905675888, 0.020699821412563324, -0.058595605194568634, 0.03138734772801399, 0.01537263672798872, -0.016859745606780052, 0.00017404808022547513, 0.0021130992099642754}; 
	const float fir_qc[NUM_TAPS] = 
		{0.030343998223543167, 0.07107564061880112, -0.13356664776802063, 0.08185412734746933, 0.004377349279820919, -0.0014515514485538006, -0.0424172505736351, 0.03763338550925255, -0.009785013273358345, -0.0001437353203073144}; 

	for (int j = 0; j < VALID_DATA_SIZE; j++) {
		float io_tmp = 0.0;
		float qo_tmp = 0.0;
		for (int t = 0; t < NUM_TAPS; t++) {
			float tmp_i_in = iTokenIn[j+t];
			float tmp_q_in = qTokenIn[j+t];
			float tmp_qc = fir_qc[NUM_TAPS - 1 - t];
			float tmp_ic = fir_ic[NUM_TAPS - 1 - t];
			io_tmp = io_tmp + tmp_i_in * tmp_ic - tmp_q_in * tmp_qc;
			qo_tmp = qo_tmp + tmp_i_in * tmp_qc + tmp_q_in * tmp_ic;
		}
		iTokenOut[j] = io_tmp;
		qTokenOut[j] = qo_tmp;
	}
	fifoReadEnd(data->shared->inputs[FIR6_I_IN]);
	fifoReadEnd(data->shared->inputs[FIR6_Q_IN]);
	fifoWriteEnd(data->shared->outputs[FIR6_I_OUT]);
	fifoWriteEnd(data->shared->outputs[FIR6_Q_OUT]);
	return p;
}

