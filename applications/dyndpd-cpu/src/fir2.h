#ifndef fir2_H
#define fir2_H

#include "common.h"

typedef struct {
	shared_t *shared;
} fir2_data_t;

int fir2Init(fir2_data_t *data);
void *fir2Fire(void *p);
void fir2Finish(fir2_data_t *data);

#endif
