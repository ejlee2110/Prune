#include "dyndpd.h"
#include "fir2.h"
#include "ports.h"

int fir2Init(fir2_data_t *data) {
	return 0;
}

void fir2Finish(fir2_data_t *data) {
}

void *fir2Fire(void *p) {

	fir2_data_t *data = (fir2_data_t *) p;
	float *iTokenIn = (float *) fifoReadStart(data->shared->inputs[FIR2_I_IN]);
	float *qTokenIn = (float *) fifoReadStart(data->shared->inputs[FIR2_Q_IN]);
	float *iTokenOut = (float *) fifoWriteStart(data->shared->outputs[FIR2_I_OUT]);
	float *qTokenOut = (float *) fifoWriteStart(data->shared->outputs[FIR2_Q_OUT]);

	const float fir_ic[NUM_TAPS] = 
		{0.1266007125377655, -0.04028431698679924, 0.013998675160109997, -0.013369462452828884, 0.01664913073182106, -0.017975620925426483, 0.015049858018755913, -0.008891353383660316, 0.0030393903143703938, -0.00015646828978788108}; 
	const float fir_qc[NUM_TAPS] = 
		{0.04336567595601082, -0.022722559049725533, 0.0252897460013628, -0.029810819774866104, 0.020235655829310417, -0.00048388622235506773, -0.013829987496137619, 0.014270305633544922, -0.0070675211027264595, 0.00205600936897099}; 

	for (int j = 0; j < VALID_DATA_SIZE; j++) {
		float io_tmp = 0.0;
		float qo_tmp = 0.0;
		for (int t = 0; t < NUM_TAPS; t++) {
			float tmp_i_in = iTokenIn[j+t];
			float tmp_q_in = qTokenIn[j+t];
			float tmp_qc = fir_qc[NUM_TAPS - 1 - t];
			float tmp_ic = fir_ic[NUM_TAPS - 1 - t];
			io_tmp = io_tmp + tmp_i_in * tmp_ic - tmp_q_in * tmp_qc;
			qo_tmp = qo_tmp + tmp_i_in * tmp_qc + tmp_q_in * tmp_ic;
		}
		iTokenOut[j] = io_tmp;
		qTokenOut[j] = qo_tmp;
	}
	fifoReadEnd(data->shared->inputs[FIR2_I_IN]);
	fifoReadEnd(data->shared->inputs[FIR2_Q_IN]);
	fifoWriteEnd(data->shared->outputs[FIR2_I_OUT]);
	fifoWriteEnd(data->shared->outputs[FIR2_Q_OUT]);
	return p;
}

