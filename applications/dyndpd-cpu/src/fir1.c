#include "dyndpd.h"
#include "fir1.h"
#include "ports.h"

int fir1Init(fir1_data_t *data) {
	return 0;
}

void fir1Finish(fir1_data_t *data) {
}

void *fir1Fire(void *p) {

	fir1_data_t *data = (fir1_data_t *) p;
	float *iTokenIn = (float *) fifoReadStart(data->shared->inputs[FIR1_I_IN]);
	float *qTokenIn = (float *) fifoReadStart(data->shared->inputs[FIR1_Q_IN]);
	float *iTokenOut = (float *) fifoWriteStart(data->shared->outputs[FIR1_I_OUT]);
	float *qTokenOut = (float *) fifoWriteStart(data->shared->outputs[FIR1_Q_OUT]);

	const float fir_ic[NUM_TAPS] =
		{1.0937772989273071, -0.22744165360927582, -0.07462470978498459, 0.03306879103183746, 0.05203430727124214, -0.024044763296842575, -0.03841766342520714, 0.039712920784950256, -0.012046308256685734, 0.0003442339366301894};
	const float fir_qc[NUM_TAPS] =
		{-0.051392920315265656, 0.01699735037982464, -0.00781444925814867, 0.01090577244758606, -0.011556293815374374, -0.0010344496695324779, 0.014542372897267342, -0.011368945240974426, 0.0017262097680941224, 0.0007368592196144164}; 

	for (int j = 0; j < VALID_DATA_SIZE; j++) {
		float io_tmp = 0.0;
		float qo_tmp = 0.0;
		for (int t = 0; t < NUM_TAPS; t++) {
			float tmp_i_in = iTokenIn[j+t];
			float tmp_q_in = qTokenIn[j+t];
			float tmp_qc = fir_qc[NUM_TAPS - 1 - t];
			float tmp_ic = fir_ic[NUM_TAPS - 1 - t];
			io_tmp = io_tmp + tmp_i_in * tmp_ic - tmp_q_in * tmp_qc;
			qo_tmp = qo_tmp + tmp_i_in * tmp_qc + tmp_q_in * tmp_ic;
		}
		iTokenOut[j] = io_tmp;
		qTokenOut[j] = qo_tmp;
	}
	fifoReadEnd(data->shared->inputs[FIR1_I_IN]);
	fifoReadEnd(data->shared->inputs[FIR1_Q_IN]);
	fifoWriteEnd(data->shared->outputs[FIR1_I_OUT]);
	fifoWriteEnd(data->shared->outputs[FIR1_Q_OUT]);
	return p;
}

