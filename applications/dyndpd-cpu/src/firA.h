#ifndef firA_H
#define firA_H

#include "common.h"

typedef struct {
	shared_t *shared;
} firA_data_t;

int firAInit(firA_data_t *data);
void *firAFire(void *p);
void firAFinish(firA_data_t *data);

#endif
