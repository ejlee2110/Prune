#include "mtx_mulf_l3.h"
#include <string.h>

int getFileSize (FILE *fp);

int mtx_mulf_l3Init(mtx_mulf_l3_data_t *data) {

	char fileName[256];
	writeInputFilepath("mulL3wgt.bin", fileName);
	readRawData (fileName, (void **) &data->wgt);
	// FIXME: what should this return?
	return 0;
}

void mtx_mulf_l3Finish(mtx_mulf_l3_data_t *data) {
	free (data->wgt);
}
	
void *mtx_mulf_l3Fire(void *p) {
	const int Mdim = MAGIC;
	const int Kdim = 1;
	const int Ndim = PATCH3SQ * FM_COUNT;
	mtx_mulf_l3_data_t *data = (mtx_mulf_l3_data_t *) p;
	cl_float *input = (cl_float *) fifoReadStart(data->shared->inputs[0]);
	cl_float *output = (cl_float *) fifoWriteStart(data->shared->outputs[0]);
	#ifdef DBGPRINT
	printf("actor mtx_mulf_l3 fires\n");
	#endif

	memset (output, 0, sizeof(cl_float) * Mdim * Kdim);
    for (int i = 0; i < Mdim; i++) {
        for (int j = 0; j < Kdim; j++) {
            for (int z = 0; z < Ndim; z++) {
                matrix(output, Kdim, i, j) += matrix(data->wgt, Ndim, i, z) * matrix(input, Kdim, z, j);
            }
        }
    }

	fifoReadEnd(data->shared->inputs[0]);
	fifoWriteEnd(data->shared->outputs[0]);

	return p;
}
