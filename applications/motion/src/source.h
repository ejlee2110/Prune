#include <stdio.h>
#include "common.h"

typedef struct {
	FILE *file;
	int length;
	int iteration;
	shared_t *shared;
	char const *fn;
} source_data_t;

void sourceInit(source_data_t *data);
void *sourceFire(void *p);
void sourceFinish(source_data_t *data);


