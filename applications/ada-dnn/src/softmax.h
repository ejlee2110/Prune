#include <stdio.h>
#include "common.h"

typedef struct {
	shared_t *shared;
	float *coeff_l4;
	float *coeff_l5;
	int iteration;
} softmax_data_t;

void softmaxInit(softmax_data_t *data);
void *softmaxFire(void *p);
void softmaxFinish(softmax_data_t *data);

