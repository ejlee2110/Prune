/****************************************************************************\

    OpenCL kernels for combined convolution+relu operations
    author: Jani Boutellier, Tampere Univ. of Technology

    Adapted from LIDE-C implementation written by Renjie Xie
    Described in "Resource-Constrained Implementation and Optimization
      of a Deep Neural Network for Vehicle Classification"
    by R. Xie, H. Huttunen, S. Lin, S. S. Bhattacharyya, J. Takala
    EUSIPCO 2016

    v. 1.1 / Jan 05, 2017

\****************************************************************************/

#include "dnn.h"

__kernel void convReluL2(
	__global float* restrict fifo_in,
	__global float* restrict fifo_out,
	__global float* restrict wgt)
{
	const int nInWidth = PATCH2 + (2 * PAD_NUM);
	const int nWidth = PATCH2;
	const int nHeight = PATCH2;
 	const int nFilterWidth = CONV2SIZE;

	#if (REPEAT == 1)
    int fm = get_global_id(0);
	int frame = 0;
	#else
	int tid = get_global_id(0);
    int fm = tid & 31;
	int frame = tid >> 5;
	#endif
    int row = get_global_id(1)*PAD_NUM;
    int col = get_global_id(2)*PAD_NUM;

	float gssample[PAD_NUM*PAD_NUM] = {0};
	for (int c = 0; c < FM_COUNT; c++) {
		for (int rst = 0; rst < PAD_NUM; rst ++) {
			for (int cst = 0; cst < PAD_NUM; cst ++) {

				for (int i = 0; i < nFilterWidth; i++) {
					for (int j = 0; j < nFilterWidth; j++) {
						gssample[rst*PAD_NUM + cst] +=
							matrix1((fifo_in + frame*TOKEN2SIZE), (c*PATCH2SQPAD), nInWidth, (row+rst+i), (col+cst+j)) * 
							matrix1(wgt, (CONV2SQ * FM_COUNT * fm + c * CONV2SQ), nFilterWidth, (nFilterWidth-1-i), (nFilterWidth-1-j));
					}
				}

			}
		}
	}

	float tmp = 0;
	tmp = tmp > gssample[0] ? tmp : gssample[0];
	tmp = tmp > gssample[1] ? tmp : gssample[1];
	tmp = tmp > gssample[2] ? tmp : gssample[2];
	tmp = tmp > gssample[3] ? tmp : gssample[3];
	matrix1((fifo_out + frame*TOKEN3SIZE), fm*PATCH3SQ, (PATCH2/TILE_NUM), (row/2), (col/2)) = tmp;
}

