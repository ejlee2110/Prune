#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "dnn.h"
#include "sink.h"

extern volatile int globalApplicationFinished;
extern volatile int globalRepetitions;

void lide_c_write(sink_data_t *data, cl_float *input_array, int m) {
    for (int i = 0; i < m; i++){
		fprintf(data->file, "%f\n", input_array[i]);
    }
}

void sinkInit(sink_data_t *data) {
	char sinkFileName[256];
	data->file = NULL;
	data->length = CLASSES;
	sprintf(sinkFileName, "%s/%s.bin", getenv("HOME"), data->fn);
	data->file = fopen (sinkFileName, "w");
	if (data->file == NULL) {
		printf("Could not open %s\n", sinkFileName);
		return;
	}
	if (globalRepetitions == 0) {
		printf("Warning: number of repetitions uninitialized\n");
	}
	data->repetitions = globalRepetitions;
	data->iteration = 0;
}

void *sinkFire(void *p) {
	sink_data_t *data = (sink_data_t *) p;
	if(data->iteration >= data->repetitions) {
		printf("Sink finished\n");
		globalApplicationFinished = 1;
	} else {
		if (data->file != NULL) {
			cl_float *input = (cl_float *) fifoReadStart(data->shared->inputs[0]);
			for (int i = 0; i < REPEAT; i++) {
				lide_c_write (data, &input[i*TOKEN5SIZE], CLASSES);
			}
			fifoReadEnd(data->shared->inputs[0]);
		}
		data->iteration += REPEAT;
	}

	return p;
}

void sinkFinish(sink_data_t *data) {
	if (data->file != NULL) {
		fflush (data->file);
		fclose (data->file);
		data->file = NULL;
	}
}

