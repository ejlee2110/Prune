#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "platform.h"

Processor *Platform::addProcessor(char *name) {
	Processor *item = new Processor();
	item->setName(name);
	item->setResourceType(PROCESSOR);
	item->addBindingPort(name); // this adds a port for binding processors to actors
	vertices->push_back(item);
	return item;
}

Shared *Platform::addShared(char *name) {
	Shared *item = new Shared();
	item->setName(name);
	item->setResourceType(SHARED);
	vertices->push_back(item);
	return item;
}

Link *Platform::addLink(char *name) {
	Link *item = new Link();
	item->setName(name);
	edges->push_back(item);
	return item;
}
