#pragma once

#include "vertex.h"

#define FIFO_GEN 1
#define FIFO_CL_READ 2
#define FIFO_CL_WRITE 3
#define FIFO_CL_READWRITE 4

class Fifo : public Vertex {
  private:
	int fifoType;
	int tokenSize;
	int initialTokens;
	int tokenRate;

  public:
	Fifo();
	~Fifo();
	void setFifoType(int type);
	int getFifoType();
	void setTokenSize(int size);
	int getTokenSize();
	void setInitialTokens(int count);
	int getInitialTokens();
	void setTokenRate(int rate);
	int getTokenRate();
	port *getFifoPort(unsigned int direction);
};
