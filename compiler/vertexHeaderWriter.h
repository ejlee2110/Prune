#pragma once

#include "fileWriter.h"
#include "network.h"

class VertexHeaderWriter : public FileWriter {
  private:
	Network *network;

  public:
	VertexHeaderWriter(Network *network);
	~VertexHeaderWriter();
	void writeFile();
};
