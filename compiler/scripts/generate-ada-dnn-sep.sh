#!/bin/sh

## Prune / compiler directory; change this if the script is moved
SRC_DIR=$(dirname $0)/..
cd $SRC_DIR

APP_NAME='ada-dnn-sep'
APP_DIR="../applications/$APP_NAME"

## Create application-specific directory for generated sources
[ -d $APP_DIR ] && mkdir -p $APP_DIR/src-gen

## Compile application using prune-compile
## parameters are: <input-platform.xml> <input-network.xml> <input-mapping.xml> <output-Makefile> <output-toplevel.c> <output-ports.h> <output-vertices.h>
bin/prune-compile ../platforms/octacore_with_gpu.xml $APP_DIR/xml/pn.xml $APP_DIR/xml/mappingCL.xml $APP_DIR/CMakeLists.txt $APP_DIR/src-gen/dnn.cpp $APP_DIR/src-gen/ports.h $APP_DIR/src-gen/vertices.h
