#pragma once

#include "actor.h"
#include "fifo.h"
#include "graph.h"
#include "link.h"

class Network : public Graph {
  private:
	void replaceBroadcast(Actor *a, port *p);

  public:
	Network(){};
	~Network(){};
	Network(const Network &obj);
	Actor *addActor(char *name);
	Fifo *addFifo(char *name);
	int getActorIndex(Vertex *x);
	int getFifoIndex(Vertex *x);
	void generateDeviceIndices(int deviceType);
	int countDeviceActors(int deviceType);
	void printActorConnections(Actor *a);
	void discoverFifoTypes();
	Link *addConnection(char *name);
	port *reconnectEdge(Edge *e, port *newPort, unsigned int direction);
	void handleConfigurationPorts();
	void computeFanOuts();
	void replaceBroadcasts();
};
