#include "mapping.h"
#include <cstdio>

Mapping::Mapping() {
	edges = new std::vector<Edge *>();
}

Mapping::~Mapping() {
	delete edges;
}

Binding *Mapping::addBinding(char *name) {
	Binding *item = new Binding();
	item->setName(name);
	edges->push_back(item);
	return item;
}

void Mapping::connectBindings(Platform *platform, Network *network) {
	if (platform && network) {
		for (unsigned int i = 0; i < edges->size(); i++) {
			unsigned int ep1h = edges->at(i)->getHashByIndex(0);
			int v1ind = network->getVertexIndex(ep1h);
			if (v1ind < 0) {
				printf("Warning: process network vertex of hash %u unknown\n", ep1h);
				printf("         Skipping the rest of the mapping process.\n");
				return;
			}
			unsigned int ep2h = edges->at(i)->getHashByIndex(1);
			int v2ind = platform->getVertexIndex(ep2h);
			if (v2ind < 0) {
				printf("Info: actor %s mapped to undefined processor\n",
					   network->getVertex(ep1h)->getName());
				continue;
			}
			if ((v1ind >= 0) && (v2ind >= 0)) {
				Binding *binding = static_cast<Binding *>(edges->at(i));
				Actor *actor = static_cast<Actor *>(network->getVertex(ep1h));
				if (actor->getDeviceType() == CL_DEVICE) {
					int dimensions = binding->getWorkGroupDimensions();
					actor->setWorkGroupDimensions(dimensions);
					for (int i = 0; i < dimensions; i++) {
						actor->setWorkGroupSize(binding->getWorkGroupSize(i), i);
						actor->setWorkItemSize(binding->getWorkItemSize(i), i);
					}
				}
				Processor *proc = static_cast<Processor *>(platform->getVertex(ep2h));
				actor->setProcessor(proc);
			}
		}
	} else {
		printf("Platform and network must be set for Mapping::connectBindings");
	}
}
