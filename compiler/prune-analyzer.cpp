#include "prune-analyzer.h"
#include "array.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>

void Analyzer::findDynamicActors(Network *network, std::vector<Actor *> *dynamic) {
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->getDynamic() != NULL) {
				dynamic->push_back(a);
			}
		}
	}
}

Actor *Analyzer::findConfigurationActor(Network *network, Actor *a) {
	Fifo *v = getFifoByName(network, a->getDynamic());
	if (v == NULL) {
		return NULL;
	}
	Vertex *p = network->getVertexByIndex(v->getPredecessor(0));
	if (p->getResourceType() == ACTOR) {
		return static_cast<Actor *>(p);
	}
	printf("Error: (findConfigurationActor) predecessor \
			%s of FIFO %s is not an actor\n",
		   p->getName(), v->getName());
	return NULL;
}

bool Analyzer::findPrecedence(Network *network, Actor *x, Actor *y) {
	int yPrecedesX = network->isPredecessor(network->getActorIndex(x), network->getActorIndex(y));
	int xPrecedesY = network->isPredecessor(network->getActorIndex(y), network->getActorIndex(x));
	if (!yPrecedesX && !xPrecedesY) {
		printf("Error: dynamic actors %s and %s are disconnected\n", x->getName(), y->getName());
	}
	if (yPrecedesX && xPrecedesY) {
		printf("Error: dynamic actors %s and %s form a loop\n", x->getName(), y->getName());
	}
	if (yPrecedesX) {
		return false;
	}
	return true;
}

void Analyzer::formDPGs(Network *network, std::vector<dGraph *> *DPGs) {
	std::vector<Actor *> *dynamic = new std::vector<Actor *>();
	std::vector<bool> *marked;
	findDynamicActors(network, dynamic);
	marked = new std::vector<bool>(dynamic->size());
	for (unsigned int i = 0; i < dynamic->size(); i++) {
		marked->at(i) = false;
	}
	for (unsigned int i = 0; i < dynamic->size(); i++) {
		for (unsigned int j = 0; j < dynamic->size(); j++) {
			if ((j != i) && (!marked->at(i) && !marked->at(j))) {
				Actor *qx = findConfigurationActor(network, dynamic->at(i));
				Actor *qy = findConfigurationActor(network, dynamic->at(j));
				if (qx == NULL) {
					printf("(formDPGs) qx is NULL!\n");
					break;
				}
				if (qy == NULL) {
					printf("(formDPGs) qy is NULL!\n");
					break;
				}
				if (qy->getIndex() == qx->getIndex()) {
					dGraph *DPG = new dGraph();
					if (findPrecedence(network, dynamic->at(i), dynamic->at(j))) {
						DPG->x = dynamic->at(i);
						DPG->y = dynamic->at(j);
					} else {
						DPG->x = dynamic->at(j);
						DPG->y = dynamic->at(i);
					}
					DPG->q = qx;
					DPG->q->setConfActor(true);
					DPGs->push_back(DPG);
					marked->at(i) = true;
					marked->at(j) = true;
				}
			}
		}
	}
	delete marked;
	delete dynamic;
}

Fifo *Analyzer::getFifoByName(Network *network, char *name) {
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v == NULL) {
			printf("(getFifoByName) v is NULL\n");
			break;
		}
		if ((v->getResourceType() == FIFO) && (strcmp(v->getName(), name) == 0)) {
			return static_cast<Fifo *>(v);
		}
	}
	printf("Error: did not find FIFO by name %s\n", name);
	return NULL;
}

void Analyzer::checkDesignRule2(Network *network, dGraph *DPG, int index) {
	bool result = false;
	printf("DPG %i: checking design rule 2 -- ", index);
	if (getFifoByName(network, DPG->x->getDynamic())->getInitialTokens() ==
		getFifoByName(network, DPG->y->getDynamic())->getInitialTokens()) {
		result = true;
	}
	if (result == true) {
		printf("passed\n");
	} else {
		printf("FAILED\n");
	}
}

// Part 1: ensuring that DC actor are static processing actors
void Analyzer::checkDesignRule3_1(Network *network, dGraph *DPG, int index) {
	bool result = true;
	printf("DPG %i: checking design rule 3(1) -- ", index);
	for (unsigned int i = 0; i < DPG->DCs->size(); i++) {
		if (DPG->DCs->at(i)->isDC) {
			for (unsigned int j = 0; j < DPG->DCs->at(i)->a->size(); j++) {
				Actor *a = DPG->DCs->at(i)->a->at(j);
				if (a->hasDynamicPorts() || a->isConfActor()) {
					printf("actor %s\n", a->getName());
					result = false;
				}
			}
		}
	}
	if (result == true) {
		printf("passed\n");
	} else {
		printf("FAILED\n");
	}
}

// Part 2: ensuring that no actor is not associated with more than 1 dynamic
// actor
void Analyzer::checkDesignRule3_2(Network *network) {
	bool result = true;
	printf("Checking design rule 3(2) -- ");
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->getPredecessorCount() > 1) {
				unsigned int *dynamic =
					(unsigned int *)malloc(sizeof(unsigned int) * network->vertexCount());
				unsigned int dynamicCount = 0;
				for (unsigned int k = 0; k < a->getPredecessorCount(); k++) {
					int bInd = network->getVertexByIndex(a->getPredecessor(k))->getPredecessor(0);
					if (static_cast<Actor *>(network->getVertexByIndex(bInd))->getDynamic() !=
						NULL) {
						if (!Array::isInArray(dynamic, dynamicCount, bInd)) {
							Array::tryAddArray(dynamic, &dynamicCount, bInd);
						}
					}
				}
				if (dynamicCount > 1) {
					printf("actor %s\n", a->getName());
					result = false;
				}
				free(dynamic);
			}
			if (a->getSuccessorCount() > 1) {
				unsigned int *dynamic =
					(unsigned int *)malloc(sizeof(unsigned int) * network->vertexCount());
				unsigned int dynamicCount = 0;
				for (unsigned int k = 0; k < a->getSuccessorCount(); k++) {
					int bInd = network->getVertexByIndex(a->getSuccessor(k))->getSuccessor(0);
					if (static_cast<Actor *>(network->getVertexByIndex(bInd))->getDynamic() !=
						NULL) {
						if (!Array::isInArray(dynamic, dynamicCount, bInd)) {
							Array::tryAddArray(dynamic, &dynamicCount, bInd);
						}
					}
				}
				if ((dynamicCount > 1) && (a->isConfActor() == false)) {
					printf("actor %s\n", a->getName());
					result = false;
				}
				free(dynamic);
			}
		}
	}
	if (result == true) {
		printf("passed\n");
	} else {
		printf("FAILED\n");
	}
}

void Analyzer::checkDesignRule4(Network *network) {
	bool result = true;
	printf("Checking design rule 4 -- ");
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->checkDesignRule4() == false) {
				result = false;
			}
		}
	}
	if (result == true) {
		printf("passed\n");
	} else {
		printf("FAILED\n");
	}
}

	/*
		before:
		(i-f1-o)-(i-e1-o)-(i-a1-o)
		after:
		(i-f1-o)-(i-e1-o)-(i-a2-o)-(i-e2-o)-(i-f2-o)-(i-e3-o)-(i-a1-o)
					mod     new       new     new      new      mod
	*/

#define STR_MAX 256

Actor *Analyzer::insertDummyActor(Network *network, Vertex *f1, Vertex *a1) {
	char buffer[STR_MAX];
	sprintf(buffer, "actor%i", network->vertexCount());
	Actor *a2 = network->addActor(buffer);
	a2->setResourceType(ACTOR);
	sprintf(buffer, "fifo%i", network->vertexCount());
	Fifo *f2 = network->addFifo(buffer);
	f2->setResourceType(FIFO);
	sprintf(buffer, "edge%i", network->edgeCount());
	Edge *e2 = network->addConnection(buffer);
	sprintf(buffer, "edge%i", network->edgeCount());
	Edge *e3 = network->addConnection(buffer);

	sprintf(buffer, "in");
	//	a2: add input port "a2_in"
	port *a2i = a2->addPort(a2->getName(), buffer);
	a2i->direction = INPUT;
	//	e2:	add output port "f2_in"
	port *e2o = e2->addPort(f2->getName(), buffer);
	e2o->direction = OUTPUT;
	//	f2: add input port "f2_in"
	port *f2i = f2->addPort(f2->getName(), buffer);
	f2i->direction = INPUT;
	//	e3:	add output port "a1_in"
	port *a1i = discoverDRP(network, static_cast<Actor *>(a1), static_cast<Fifo *>(f1), INPUT);
	port *e3o = e3->addPort(a1i->name);
	e3o->direction = OUTPUT;

	sprintf(buffer, "out");
	//	a2:	add output port "a2_out"
	port *a2o = a2->addPort(a2->getName(), buffer);
	a2o->direction = OUTPUT;
	//	e2: add input port "a2_out"
	port *e2i = e2->addPort(a2->getName(), buffer);
	e2i->direction = INPUT;
	//	f2:	add output port "f2_out"
	port *f2o = f2->addPort(f2->getName(), buffer);
	f2o->direction = OUTPUT;
	//	e3: add input port "f2_out"
	port *e3i = e3->addPort(f2->getName(), buffer);
	e3i->direction = INPUT;

	//	e1: modify output port to hold "a2_in"
	char e1oName[STR_MAX];
	for (unsigned int i = 0; i < f1->portCount(); i++) {
		if (f1->getPortDirection(f1->getPort(i)->hash) == OUTPUT) {
			port *f1o = f1->getPort(i);
			Edge *e1 = network->getEdgeByPortHash(f1o->hash);
			port *e1o = e1->getOtherEndByHash(f1o->hash);
			strcpy(e1oName, e1o->name);
			network->reconnectEdge(e1, a2i, OUTPUT);
		}
	}

	//  f1: modify successor to be "a2"
	f1->setSuccessor(0, network->getActorIndex(a2));

	//	a2:	add predecessor "f1"
	a2->addPredecessor(network->getFifoIndex(f1));
	//	a2:	add successor "f2"
	a2->addSuccessor(network->getFifoIndex(f2));

	//	f2:	add predecessor "a2"
	f2->addPredecessor(network->getActorIndex(a2));
	//	f2:	add successor "a1"
	f2->addSuccessor(network->getActorIndex(a1));

	//	a1: modify predecessor to be "f2"
	for (unsigned int i = 0; i < a1->getPredecessorCount(); i++) {
		if (((int)a1->getPredecessor(i)) == network->getFifoIndex(f1)) {
			a1->setPredecessor(i, network->getFifoIndex(f2));
		}
	}

	return a2;
}

void Analyzer::findEmptyDCs(Network *network, dGraph *DPG) {
	for (unsigned int i = 0; i < DPG->x->getSuccessorCount(); i++) {
		Vertex *f = network->getVertexByIndex(DPG->x->getSuccessor(i));
		for (unsigned int j = 0; j < f->getSuccessorCount(); j++) {
			if (f->getSuccessor(j) == (unsigned int)network->getActorIndex(DPG->y)) {
#ifdef DEBUG
				Actor *d = insertDummyActor(network, f, DPG->y);
				printf("Info: DPG actor %s is directly connected to %s -- inserted "
					   "dummy actor '%s'\n",
					   DPG->x->getName(), DPG->y->getName(), d->getName());
#else
				insertDummyActor(network, f, DPG->y);
#endif
			}
		}
	}
}

port *Analyzer::discoverDRP(Network *network, Actor *x, Fifo *f, int direction) {
	for (unsigned int i = 0; i < x->portCount(); i++) {
		if (x->getPortDirection(x->getPort(i)->hash) == direction) {
			port *xo = x->getPort(i);
			port *eo = network->getEdgeByPortHash(xo->hash)->getOtherEndByHash(xo->hash);
			for (unsigned int i = 0; i < f->portCount(); i++) {
				if (f->getPortDirection(f->getPort(i)->hash) == (3 - direction)) {
					if (f->getPort(i)->hash == eo->hash) {
						return xo;
					}
				}
			}
		}
	}
	return NULL;
}

int Analyzer::compareVertexIndices(Network *network, Vertex *v, std::vector<pfPair *> *DRPs) {
	for (unsigned int i = 0; i < DRPs->size(); i++) {
		if (network->getFifoIndex(DRPs->at(i)->v) == network->getFifoIndex(v)) {
			return (int)i;
		}
	}
	return -1;
}

void Analyzer::printDC(dC *DC, bool printPorts) {
	for (unsigned int k = 0; k < DC->a->size(); k++) {
		printf("%s ", DC->a->at(k)->getName());
	}
	if (printPorts) {
		printf("\n inputs: ");
		for (unsigned int k = 0; k < DC->DRPx->size(); k++) {
			printf("%s ", DC->DRPx->at(k)->name);
		}
		printf("\n outputs: ");
		for (unsigned int k = 0; k < DC->DRPy->size(); k++) {
			printf("%s ", DC->DRPy->at(k)->name);
		}
	}
	printf("\n");
}

// If the construction of DCs by this function succeeds, the hosting
// DPG implicitly fulfills Design Rule 5 and Design Rule 1
void Analyzer::formDCs(Network *network, dGraph *DPG, int index) {
	bool DCsValid = true;
	int subGraphCount = 1;
	unsigned int *subGraphs = (unsigned int *)malloc(sizeof(unsigned int) * network->vertexCount());
	std::vector<pfPair *> DRPsX, DRPsY;
	for (unsigned int i = 0; i < DPG->x->getSuccessorCount(); i++) {
		Vertex *f = network->getVertexByIndex(DPG->x->getSuccessor(i));
		if (network->isSuccessor(network->getFifoIndex(f), network->getActorIndex(DPG->y))) {
			pfPair *newPair = new pfPair();
			newPair->v = f;
			newPair->DRP = discoverDRP(network, DPG->x, static_cast<Fifo *>(f), OUTPUT);
			newPair->DRP->isDRP = true;
			DRPsX.push_back(newPair);
		}
	}
	for (unsigned int i = 0; i < DPG->y->getPredecessorCount(); i++) {
		Vertex *f = network->getVertexByIndex(DPG->y->getPredecessor(i));
		if (network->isPredecessor(network->getFifoIndex(f), network->getActorIndex(DPG->x))) {
			pfPair *newPair = new pfPair();
			newPair->v = f;
			newPair->DRP = discoverDRP(network, DPG->y, static_cast<Fifo *>(f), INPUT);
			newPair->DRP->isDRP = true;
			DRPsY.push_back(newPair);
		}
	}
	for (int i = DRPsX.size() - 1; i >= 0; i--) {
		Vertex *f = DRPsX.at(i)->v;
		DPG->x->removeSuccessor(network->getFifoIndex(f));
		f->removePredecessor(f->getPredecessor(0));
	}
	for (int i = DRPsY.size() - 1; i >= 0; i--) {
		Vertex *f = DRPsY.at(i)->v;
		DPG->y->removePredecessor(network->getFifoIndex(f));
		f->removeSuccessor(f->getSuccessor(0));
	}
	subGraphCount = network->findDisconnectedSubgraphs(subGraphs);
#ifdef DEBUG
	printf("Info: DPG %s-%s-%s appears to have %i DCs\n", DPG->q->getName(), DPG->x->getName(),
		   DPG->y->getName(), subGraphCount - 1);
#endif
	DPG->DCs = new std::vector<dC *>();
	for (int i = 0; i < subGraphCount; i++) {
		unsigned int *DCinds =
			(unsigned int *)malloc(sizeof(unsigned int) * network->vertexCount());
		unsigned int DCSize = network->getMaximalSubgraph(DCinds, subGraphs[i]);
		dC *thisDC = new dC();
		thisDC->a = new std::vector<Actor *>();
		thisDC->DRPx = new std::vector<port *>();
		thisDC->DRPy = new std::vector<port *>();
		for (unsigned int j = 0; j < DCSize; j++) {
			Vertex *v = network->getVertexByIndex(DCinds[j]);
			if (v->getResourceType() == ACTOR) {
				thisDC->a->push_back(static_cast<Actor *>(v));
			} else {
				int indX = compareVertexIndices(network, v, &DRPsX);
				if (indX > -1) {
					thisDC->DRPx->push_back(DRPsX.at(indX)->DRP);
				}
				int indY = compareVertexIndices(network, v, &DRPsY);
				if (indY > -1) {
					thisDC->DRPy->push_back(DRPsY.at(indY)->DRP);
				}
			}
		}
		if ((thisDC->DRPx->size() == 0) && (thisDC->DRPy->size() == 0)) {
			/*printf("Info: host graph of DPG %s-%s-%s has actors: ",
				DPG->q->getName(), DPG->x->getName(), DPG->y->getName());
			printDC (thisDC, false);*/
			thisDC->isDC = false;
		} else {
			if (thisDC->DRPx->size() * thisDC->DRPy->size() == 0) {
				printf("Warning: Subgraph ");
				printDC(thisDC, true);
				printf(" misses input or output DRPs -- not a host graph, nor a DC.\n");
				DCsValid = false;
			} else {
				thisDC->isDC = true;
			}
		}
		DPG->DCs->push_back(thisDC);
		free(DCinds);
	}
	printf("DPG %i: design rule 1 -- ", index);
	if ((subGraphCount > 0) && DCsValid) {
		printf("passed\n");
	} else {
		if (subGraphCount == 0) {
			printf("due to 0 DCs: \n");
		}
		printf("FAILED\n");
	}
	printf("DPG %i: design rule 5 -- ", index);
	if ((subGraphCount > 0) && DCsValid) {
		printf("passed\n");
	} else {
		if (subGraphCount == 0) {
			printf("due to 0 DCs: \n");
		}
		printf("FAILED\n");
	}
	for (unsigned int i = 0; i < DRPsX.size(); i++) {
		delete DRPsX.at(i);
	}
	for (unsigned int i = 0; i < DRPsY.size(); i++) {
		delete DRPsY.at(i);
	}
	free(subGraphs);
}

void Analyzer::writeControlFunction(Network *network, dGraph *DPG, Actor *a, bool isX, char *dir) {
	char buffer[STR_MAX];
	sprintf(buffer, "%s/%s.c", dir, a->getName());
	ControlFunctionWriter *cw = new ControlFunctionWriter(network);
	if (cw->openFile(buffer)) {
		cw->writeFile(DPG, a, isX);
		cw->closeFile();
		printf("Info: Produced control function for actor %s\n", a->getName());
	}
	delete cw;
}

void Analyzer::formDCMaps(Network *network, char *dir) {
	Network *networkCopy = new Network(*network);
	std::vector<dGraph *> *DPGs = new std::vector<dGraph *>();
	formDPGs(networkCopy, DPGs);
	for (unsigned int i = 0; i < DPGs->size(); i++) {
#ifdef DEBUG
		printf("Info: DPG %i is %s-%s-%s\n", i, DPGs->at(i)->q->getName(),
			   DPGs->at(i)->x->getName(), DPGs->at(i)->y->getName());
#endif
		checkDesignRule2(network, DPGs->at(i), i);
		findEmptyDCs(networkCopy, DPGs->at(i));
		formDCs(networkCopy, DPGs->at(i), i);
		/*for (unsigned int j = 0; j < DPGs->at(i)->DCs->size(); j++) {
			if (DPGs->at(i)->DCs->at(j)->isDC) {
				printf(" DC %i: ", j);
				printDC (DPGs->at(i)->DCs->at(j), true);
			}
		}*/
		checkDesignRule3_1(networkCopy, DPGs->at(i), i);
		writeControlFunction(networkCopy, DPGs->at(i), DPGs->at(i)->x, true, dir);
		writeControlFunction(networkCopy, DPGs->at(i), DPGs->at(i)->y, false, dir);
	}
	for (unsigned int i = 0; i < DPGs->size(); i++) {
		for (unsigned int j = 0; j < DPGs->at(i)->DCs->size(); j++) {
			delete DPGs->at(i)->DCs->at(j)->a;
			delete DPGs->at(i)->DCs->at(j)->DRPx;
			delete DPGs->at(i)->DCs->at(j)->DRPy;
		}
		delete DPGs->at(i)->DCs;
		delete DPGs->at(i);
	}
	delete DPGs;
	delete networkCopy;
}

void Analyzer::doAnalysis(Network *network, char *dir) {
	std::vector<dGraph *> *DPGs = new std::vector<dGraph *>();
	formDPGs(network, DPGs);
	checkDesignRule3_2(network);
	checkDesignRule4(network);
	formDCMaps(network, dir);
	for (unsigned int i = 0; i < DPGs->size(); i++) {
		delete DPGs->at(i);
	}
	delete DPGs;
}
