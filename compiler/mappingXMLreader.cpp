#include "mappingXMLreader.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>

void MappingXMLReader::storeOpenCL(Binding *link, xmlTextReaderPtr reader) {
	unsigned char *targetGroups = xmlTextReaderGetAttribute(reader, (unsigned char *)"workgroups");
	unsigned char *targetItems = xmlTextReaderGetAttribute(reader, (unsigned char *)"workitems");
	if (targetGroups != NULL) {
#ifdef DEBUG
		printf("storeOpenCL: workgroups:%s\n", targetGroups);
#endif
		link->setWorkGroups((char *)targetGroups);
	}
	if (targetItems != NULL) {
#ifdef DEBUG
		printf("storeOpenCL: workitems:%s\n", targetItems);
#endif
		link->setWorkItems((char *)targetItems);
	}
	free(targetGroups);
	free(targetItems);
}

void MappingXMLReader::readTargetDetails(Binding *link, xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);

	if (xmlTextReaderDepth(reader) == 3) {
		if (!strcmp((char *)name, "opencl")) {
			storeOpenCL(link, reader);
		} else if ((xmlTextReaderNodeType(reader) == TEXTSKIP) ||
				   (xmlTextReaderNodeType(reader) == COMMENT)) {
		} else {
			printf("MappingXMLReader::readTargetDetails: unknown name %s encountered\n",
				   (char *)name);
		}
	}
}

void MappingXMLReader::storeTarget(Binding *link, xmlTextReaderPtr reader) {
#ifdef DEBUG
	printf("Target found\n");
#endif
	int ret = xmlTextReaderRead(reader);
	while (ret == 1) {
		if (xmlTextReaderDepth(reader) == 2) {
			break;
		}
		readTargetDetails(link, reader);
		ret = xmlTextReaderRead(reader);
	}
}

void MappingXMLReader::storeEndPoint(Mapping *mapping, Binding *link, xmlTextReaderPtr reader,
									 int num) {
	unsigned char *pointName = xmlTextReaderGetAttribute(reader, (unsigned char *)"name");
	if (pointName != NULL) {
#ifdef DEBUG
		printf("Endpoint %s found\n", pointName);
#endif
		link->addBindingPort((char *)pointName);
	}
	free(pointName);
}

void MappingXMLReader::readBindingDetails(Mapping *mapping, Binding *link,
										  xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 2) {
		if (!strcmp((char *)name, "process")) {
			storeEndPoint(mapping, link, reader, 1);
		} else if (!strcmp((char *)name, "processor")) {
			storeEndPoint(mapping, link, reader, 2);
		} else if (!strcmp((char *)name, "target")) {
			storeTarget(link, reader);
		} else if ((xmlTextReaderNodeType(reader) == TEXTSKIP) ||
				   (xmlTextReaderNodeType(reader) == COMMENT)) {
		} else {
			printf("MappingXMLReader::unknown name %s encountered\n", (char *)name);
		}
	}
}

void MappingXMLReader::storeBinding(Mapping *mapping, xmlTextReaderPtr reader) {
	unsigned char *linkName = xmlTextReaderGetAttribute(reader, (unsigned char *)"name");
	if (linkName != NULL) {
#ifdef DEBUG
		printf("Binding %s found\n", linkName);
#endif
		Binding *link = mapping->addBinding((char *)linkName);
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 1) {
				break;
			}
			readBindingDetails(mapping, link, reader);
			ret = xmlTextReaderRead(reader);
		}
	}
	free(linkName);
}

void MappingXMLReader::readMappingDetails(Mapping *mapping, xmlTextReaderPtr reader) {
	const unsigned char *name = xmlTextReaderConstName(reader);
	if (xmlTextReaderDepth(reader) == 1) {
		if (!strcmp((char *)name, "binding")) {
			storeBinding(mapping, reader);
		} else if ((xmlTextReaderNodeType(reader) == TEXTSKIP) ||
				   (xmlTextReaderNodeType(reader) == COMMENT)) {
		} else {
			printf("MappingXMLReader::unknown name %s encountered\n", (char *)name);
		}
	}
}

Mapping *MappingXMLReader::storeMapping(xmlTextReaderPtr reader) {
	Mapping *mapping = NULL;
	unsigned char *mappingName = xmlTextReaderGetAttribute(reader, (unsigned char *)"name");
	if (mappingName != NULL) {
#ifdef DEBUG
		printf("Mapping %s found\n", mappingName);
#endif
		mapping = new Mapping();
		mapping->setName((char *)mappingName);
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			if (xmlTextReaderDepth(reader) == 0) {
				break;
			}
			readMappingDetails(mapping, reader);
			ret = xmlTextReaderRead(reader);
		}
	}
	free(mappingName);
	return mapping;
}

Mapping *MappingXMLReader::readMapping(const char *fileName) {
	Mapping *mapping = NULL;

	LIBXML_TEST_VERSION

	xmlTextReaderPtr reader = xmlReaderForFile(fileName, NULL, 0);

	if (reader != NULL) {
		int ret = xmlTextReaderRead(reader);
		while (ret == 1) {
			const unsigned char *name = xmlTextReaderConstName(reader);
			if (!strcmp((char *)name, "mapping")) {
				mapping = storeMapping(reader);
			} else if ((xmlTextReaderNodeType(reader) == TEXTSKIP) ||
					   (xmlTextReaderNodeType(reader) == COMMENT)) {
			} else {
				printf("MappingXMLReader::unknown name %s encountered\n", (char *)name);
			}
			ret = xmlTextReaderRead(reader);
		}
		xmlFreeTextReader(reader);
	} else {
		printf("Unable to open %s\n", fileName);
	}

	xmlCleanupParser();
	xmlMemoryDump();

	return mapping;
}
