#include "edge.h"
#include <cstdio>

Edge::Edge(const Edge &obj) : Entity(obj) {}

port *Edge::getOtherEndByHash(unsigned int hash) {
	if (portCount() != 2) {
		printf("Error: edge %s has %i ports (should have exactly 2)\n", name, portCount());
		return NULL;
	} else if (getPort(0)->hash == hash) {
		return getPort(1);
	}
	return getPort(0);
}
