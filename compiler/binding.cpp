#include "binding.h"
#include <cstdlib>
#include <cstring>

Binding::Binding() : dimensions(0) {
	workGroups[0] = 0;
	workGroups[1] = 0;
	workGroups[2] = 0;
	workItems[0] = 0;
	workItems[1] = 0;
	workItems[2] = 0;
}

Binding::~Binding() {}

int Binding::countWGDimensions(char *wgs) {
	int cnt = 0;
	for (unsigned int i = 0; i < strlen(wgs); i++) {
		if (wgs[i] == ';') {
			cnt++;
		}
	}
	return cnt + 1;
}

void Binding::setWorkGroups(char *wgs) {
	dimensions = countWGDimensions(wgs);
	int cnt = 0;
	char *pch;
	pch = strtok(wgs, ";");
	while ((pch != NULL) && (cnt < dimensions)) {
		workGroups[cnt] = atoi(pch);
		pch = strtok(NULL, ";");
		cnt++;
	}
}

int Binding::getWorkGroupDimensions() {
	return dimensions;
}

int Binding::getWorkGroupSize(int dimension) {
	return workGroups[dimension];
}

void Binding::setWorkItems(char *wis) {
	dimensions = countWGDimensions(wis);
	int cnt = 0;
	char *pch;
	pch = strtok(wis, ";");
	while ((pch != NULL) && (cnt < dimensions)) {
		workItems[cnt] = atoi(pch);
		pch = strtok(NULL, ";");
		cnt++;
	}
}

int Binding::getWorkItemSize(int dimension) {
	return workItems[dimension];
}

