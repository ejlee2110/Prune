#include "hash.h"
#include "array.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>

unsigned int Hash::hashCharArray(char *arr, unsigned int len) {
	unsigned int hash = 5381;
	for (unsigned int i = 0; i < len; i++) {
		hash = ((hash << 5) + hash) + arr[i];
	}
	return hash;
}

unsigned int Hash::hashCharArray(char *arr, unsigned int len, unsigned int seed) {
	unsigned int hash = seed;
	for (unsigned int i = 0; i < len; i++) {
		hash = ((hash << 5) + hash) + arr[i];
	}
	return hash;
}

unsigned int Hash::hashIntArray(unsigned int *arr, unsigned int len) {
	unsigned int hash = 5381;
	for (unsigned int i = 0; i < len; i++) {
		hash = ((hash << 5) + hash) + arr[i];
	}
	return hash;
}

unsigned int Hash::hashIntArraySorted(unsigned int *arr, unsigned int len) {
	unsigned int *hashList = (unsigned int *)malloc(len * sizeof(unsigned int));
	memcpy(hashList, arr, len * sizeof(unsigned int));
	Array::sortArray(hashList, len);
	unsigned int hash = hashIntArray(hashList, len);
	free(hashList);
	return hash;
}
