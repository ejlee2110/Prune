#include "processor.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>

Processor::Processor() : processorType(0), coreIndex(-1) {
	capabilities = new std::vector<capability *>();
}

Processor::~Processor() {
	for (unsigned int i = 0; i < capabilities->size(); i++) {
		capability *cap = capabilities->at(i);
		free(cap->name);
		free(cap->id);
		delete cap;
	}
	delete capabilities;
}

void Processor::setProcessorType(int type) {
	processorType = type;
}

void Processor::extractCoreId() {
	if (name != NULL) {
		int tmp;
		int retVal = sscanf(name, "core_%i\n", &tmp);
		if (retVal != 1) {
			printf("Warning: Processor id %s does not match expected format core_N\n", name);
		} else {
			coreIndex = tmp;
		}
	}
}

int Processor::getCoreId() {
	return coreIndex;
}

int Processor::getProcessorType() {
	return processorType;
}

void Processor::addCapability(char *name, char *id) {
	capability *cap = new capability;
	cap->name = (char *)malloc(strlen(name) + 1);
	cap->id = (char *)malloc(strlen(id) + 1);
	strcpy(cap->name, name);
	strcpy(cap->id, id);
	capabilities->push_back(cap);
}
