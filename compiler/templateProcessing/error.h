#pragma once

#include <string>

namespace TemplateProcessing {

const int NO_ERROR = 0;
const int FILE_NOT_FOUND = 1;
const int UNKNOWN_ERROR = 2;
const int INVALID_PARAMETER = 3;

struct Error {
	int code;
	std::string str;
};

/*
 * \brief Plain constructor for Error.
 */
Error make_error(int code, std::string const &errorString);

} // namespace TemplateProcessing
