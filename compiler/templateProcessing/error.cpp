#include "error.h"

namespace TemplateProcessing {

Error make_error(int code, std::string const &errorString) {
	Error e = {code, errorString};
	return e;
}

} // namespace TemplateProcessing
