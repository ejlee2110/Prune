#pragma once

#include "entity.h"

#define PROCESSOR 0
#define SHARED 1
#define ACTOR 2
#define FIFO 3

class Vertex : public Entity {
  protected:
	std::vector<unsigned int> *predecessors;
	std::vector<unsigned int> *successors;
	int resourceType;
	int index;

	Vertex();
	~Vertex();
	void setUniqueName(char *str);

  public:
	Vertex(const Vertex &obj);
	unsigned int getPredecessorCount();
	unsigned int getSuccessorCount();
	unsigned int getPredecessor(unsigned int predecessorIndex);
	unsigned int getSuccessor(unsigned int successorIndex);
	void setPredecessor(unsigned int predecessorIndex, unsigned int newPredecessor);
	void setSuccessor(unsigned int successorIndex, unsigned int newSuccessor);
	int getResourceType();
	void setResourceType(int type);
	void addSuccessor(unsigned int vertexIndex);
	void addPredecessor(unsigned int vertexIndex);
	void removeSuccessor(unsigned int vertexIndex);
	void removePredecessor(unsigned int vertexIndex);
	void addConnected(unsigned int vertexIndex);
	void setIndex(int index);
	int getIndex();
};
