#include "graph.h"
#include "array.h"
#include "hash.h"
#include <cstdio>
#include <cstdlib>

Graph::Graph() {
	vertices = new std::vector<Vertex *>();
	edges = new std::vector<Edge *>();
}

Graph::~Graph() {
	delete vertices;
	delete edges;
}

Graph::Graph(const Graph &obj) : Entity(obj) {
	vertices = new std::vector<Vertex *>();
	edges = new std::vector<Edge *>();
	for (unsigned int i = 0; i < obj.vertices->size(); i++) {
		vertices->push_back(new Vertex(*obj.vertices->at(i)));
	}
	for (unsigned int i = 0; i < obj.edges->size(); i++) {
		edges->push_back(new Edge(*obj.edges->at(i)));
	}
}

unsigned int Graph::vertexCount() {
	return vertices->size();
}

unsigned int Graph::edgeCount() {
	return edges->size();
}

Vertex *Graph::getVertex(unsigned int hash) {
	for (unsigned int i = 0; i < vertices->size(); i++) {
		for (unsigned int j = 0; j < vertices->at(i)->portCount(); j++) {
			if (vertices->at(i)->getHashByIndex(j) == hash) {
				return vertices->at(i);
			}
		}
	}
	return NULL;
}

int Graph::getVertexIndex(unsigned int hash) {
	for (unsigned int i = 0; i < vertices->size(); i++) {
		for (unsigned int j = 0; j < vertices->at(i)->portCount(); j++) {
			if (vertices->at(i)->getHashByIndex(j) == hash) {
				return i;
			}
		}
	}
	return -1;
}

Vertex *Graph::getVertexByIndex(unsigned int index) {
	return vertices->at(index);
}

Vertex *Graph::getVertexByOrder(int order, int type) {
	for (unsigned int i = 0; i < vertices->size(); i++) {
		Vertex *v = vertices->at(i);
		if (v->getResourceType() == type) {
			if (v->getIndex() == order) {
				return v;
			}
		}
	}
	printf("Error: no vertex of type %i found by order #%i\n", type, order);
	return NULL;
}

Edge *Graph::getEdgeByPortHash(unsigned int hash) {
	for (unsigned int j = 0; j < edges->size(); j++) {
		for (unsigned int k = 0; k < edges->at(j)->portCount(); k++) {
			if (edges->at(j)->getPort(k)->hash == hash) {
				return edges->at(j);
			}
		}
	}
	printf("Error: no edge found with port hash #%i\n", hash);
	return NULL;
}

unsigned int Graph::resourceCount(int type) {
	unsigned int resourceCount = 0;
	for (unsigned int i = 0; i < vertices->size(); i++) {
		Vertex *v = vertices->at(i);
		if (v->getResourceType() == type) {
			resourceCount++;
		}
	}
	return resourceCount;
}

void Graph::connectUndirectedGraph() {
	for (unsigned int i = 0; i < edges->size(); i++) {
		unsigned int ep1h = edges->at(i)->getHashByIndex(0);
		// unsigned int v1ind = getVertexIndex(ep1h);
		int v1ind = getVertexIndex(ep1h);
		if (v1ind < 0) {
			printf("Warning: (connectUndirectedGraph) vertex of hash %u unknown\n", ep1h);
			return;
		}
		unsigned int ep2h = edges->at(i)->getHashByIndex(1);
		// unsigned int v2ind = getVertexIndex(ep2h);
		int v2ind = getVertexIndex(ep2h);
		if (v2ind < 0) {
			printf("Warning: (connectUndirectedGraph) vertex of hash %u unknown\n", ep2h);
			return;
		}
		if ((v1ind >= 0) && (v2ind >= 0)) {
			getVertex(ep1h)->addConnected(v2ind);
			getVertex(ep2h)->addConnected(v1ind);
		}
	}
}

void Graph::connectDirectedGraph() {
	for (unsigned int i = 0; i < edges->size(); i++) {
		unsigned int ep1h = edges->at(i)->getHashByIndex(0);
		int v1ind = getVertexIndex(ep1h);
		if (v1ind < 0) {
			printf("Warning1: (connectDirectedGraph) vertex of hash %u unknown\n", ep1h);
			Vertex *v = getVertex(ep1h);
			if (v != NULL) {
				printf("         (Vertex name is %s)\n", v->getName());
			}
			return;
		}
		int ep1d = getVertex(ep1h)->getPortDirection(ep1h);
		unsigned int ep2h = edges->at(i)->getHashByIndex(1);
		int v2ind = getVertexIndex(ep2h);
		if (v2ind < 0) {
			printf("Warning2: (connectDirectedGraph) vertex of hash %u unknown\n", ep2h);
			printf("         (Source vertex name is %s)\n", getVertex(ep1h)->getName());
			Vertex *v = getVertex(ep2h);
			if (v != NULL) {
				printf("         (Problem (sink) vertex name is %s)\n", v->getName());
			}
			return;
		}
		int ep2d = getVertex(ep2h)->getPortDirection(ep2h);
		if ((v1ind >= 0) && (v2ind >= 0)) {
			// the & sign is to accept both INPUT and CONFIGURATION ports
			if ((ep1d == OUTPUT) && (ep2d & INPUT)) {
				getVertex(ep1h)->addSuccessor(v2ind);
				getVertex(ep2h)->addPredecessor(v1ind);
			} else if ((ep1d & INPUT) && (ep2d == OUTPUT)) {
				getVertex(ep1h)->addPredecessor(v2ind);
				getVertex(ep2h)->addSuccessor(v1ind);
			} else {
				printf("Error: port %u of vertex %s has direction %i and\n", ep1h,
					   getVertex(ep1h)->getName(), ep1d);
				printf("       port %u of vertex %s has direction %i\n", ep2h,
					   getVertex(ep2h)->getName(), ep2d);
			}
		}
	}
}

void Graph::initGraphTraversal(traversal *t, unsigned int origin) {
	t->found = (unsigned int *)malloc(sizeof(unsigned int) * vertices->size());
	t->origin = origin;
	t->foundCount = 0;
}

void Graph::deinitGraphTraversal(traversal *t) {
	free(t->found);
	t->found = NULL;
	t->foundCount = 0;
}

void Graph::findConnectedVertices(traversal *t, int currInd) {
	unsigned int i;

	if (Array::isInArray(t->found, t->foundCount, currInd))
		return;

	Array::tryAddArray(t->found, &t->foundCount, currInd);

	for (i = 0; i < vertices->at(currInd)->getPredecessorCount(); i++) {
		findConnectedVertices(t, vertices->at(currInd)->getPredecessor(i));
	}

	for (i = 0; i < vertices->at(currInd)->getSuccessorCount(); i++) {
		findConnectedVertices(t, vertices->at(currInd)->getSuccessor(i));
	}
}

int Graph::getMaximalSubgraph(unsigned int *output, unsigned int origin) {
	unsigned int subGraphSize;
	traversal t;
	initGraphTraversal(&t, origin);
	findConnectedVertices(&t, origin);
	subGraphSize = t.foundCount;
	for (unsigned int i = 0; i < subGraphSize; i++) {
		output[i] = t.found[i];
	}
	deinitGraphTraversal(&t);
	return subGraphSize;
}

// This function counts the number of disconnected subgraphs, returns a
// list of nodes. Each entry in the list is the index of an arbitrary node
// from each disconnected graph instance.
int Graph::findDisconnectedSubgraphs(unsigned int *output) {
	unsigned int hashCount = 0;
	unsigned int *hashList;
	unsigned int listLen = vertices->size();

	hashList = (unsigned int *)malloc(listLen * sizeof(unsigned int));

	for (unsigned int i = 0; i < listLen; i++) {
		unsigned int hash;
		traversal t;
		initGraphTraversal(&t, i);
		findConnectedVertices(&t, i);
		hash = Hash::hashIntArraySorted(t.found, t.foundCount);
		if (Array::tryAddArray(hashList, &hashCount, hash)) {
			output[hashCount - 1] = t.found[0];
		}
		deinitGraphTraversal(&t);
	}

	free(hashList);

	return hashCount;
}

void Graph::dumpPortData() {
	for (unsigned int i = 0; i < vertices->size(); i++) {
		for (unsigned int j = 0; j < vertices->at(i)->portCount(); j++) {
			printf("Vertex %s port %s (%u) direction %i\n", vertices->at(i)->getName(),
				   vertices->at(i)->getPort(j)->name, vertices->at(i)->getPort(j)->hash,
				   vertices->at(i)->getPort(j)->direction);
		}
	}
}

void Graph::findPredecessors(traversal *t, unsigned int currInd) {
	if (Array::isInArray(t->found, t->foundCount, currInd)) {
		return;
	}
	Array::tryAddArray(t->found, &t->foundCount, currInd);
	for (unsigned int i = 0; i < vertices->at(currInd)->getPredecessorCount(); i++) {
		int pred = vertices->at(currInd)->getPredecessor(i);
		findPredecessors(t, pred);
	}
}

void Graph::findSuccessors(traversal *t, unsigned int currInd) {
	if (Array::isInArray(t->found, t->foundCount, currInd)) {
		return;
	}
	Array::tryAddArray(t->found, &t->foundCount, currInd);
	for (unsigned int i = 0; i < vertices->at(currInd)->getSuccessorCount(); i++)
		findSuccessors(t, vertices->at(currInd)->getSuccessor(i));
}

int Graph::isPredecessor(int orgId, int trgId) {
	int count;
	traversal others;
	initGraphTraversal(&others, orgId);
	findPredecessors(&others, orgId);
	count = Array::isInArray(others.found, others.foundCount, trgId);
	deinitGraphTraversal(&others);
	return count;
}

int Graph::isSuccessor(int orgId, int trgId) {
	int count;
	traversal others;
	initGraphTraversal(&others, orgId);
	findSuccessors(&others, orgId);
	count = Array::isInArray(others.found, others.foundCount, trgId);
	deinitGraphTraversal(&others);
	return count;
}

// Sorts nodes given in node_list to an order such that
// node_list[0] has no predecessors (considering ones in the list)
// and node_list[list_len] has no successors
void Graph::sortNodes(int *nodeList, int listLen) {
	int deadlockCounter = MAX_ITERATIONS; // defined in array.h
	int foundUnsorted = 1;

	while ((foundUnsorted == 1) && (deadlockCounter > 0)) {
		int i;
		int tmp;
		foundUnsorted = 0;
		for (i = 0; i < listLen - 1; i++) {
			if (isPredecessor(nodeList[i], nodeList[i + 1])) {
				foundUnsorted = 1;
				break;
			}
		}

		if (foundUnsorted) {
			tmp = nodeList[i];
			nodeList[i] = nodeList[i + 1];
			nodeList[i + 1] = tmp;
		}

		deadlockCounter--;
	}
	if (deadlockCounter == 0) {
		printf("*** Error: deadlock in Graph::sortNodes\n");
	}
}

void Graph::printPredecessors() {
	for (unsigned int i = 0; i < vertices->size(); i++) {
		printf("Predecessors of %s:", vertices->at(i)->getName());
		for (unsigned int j = 0; j < vertices->at(i)->getPredecessorCount(); j++) {
			printf("%s ", vertices->at(vertices->at(i)->getPredecessor(j))->getName());
		}
		printf("\n");
	}
}

void Graph::printSuccessors(Vertex *v) {
	printf("Successors of %s:", v->getName());
	for (unsigned int j = 0; j < v->getSuccessorCount(); j++) {
		printf("%s ", vertices->at(v->getSuccessor(j))->getName());
	}
	printf("\n");
}

void Graph::printSuccessors() {
	for (unsigned int i = 0; i < vertices->size(); i++) {
		printf("%i ", i);
		printSuccessors(vertices->at(i));
	}
}

void Graph::generateIndices() {
	int fifoIndex = 0;
	int actorIndex = 0;
	int *nodeList = (int *)malloc(sizeof(int) * vertices->size());

	for (unsigned int i = 0; i < vertices->size(); i++) {
		nodeList[i] = i;
	}

	sortNodes(nodeList, vertices->size());

	for (unsigned int i = 0; i < vertices->size(); i++) {
		Vertex *v = vertices->at(nodeList[i]);
		if (v->getResourceType() == FIFO) {
			v->setIndex(fifoIndex++);
		} else if (v->getResourceType() == ACTOR) {
			v->setIndex(actorIndex++);
		}
	}
	free(nodeList);
}
